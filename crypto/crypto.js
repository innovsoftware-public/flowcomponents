exports.id = 'cryptohash';
exports.title = 'Crypto Hash';
exports.group = 'Encrypt';
exports.color = '#37BC9B';
exports.input = true;
exports.output = 1;
exports.author = 'Leonardo Alves';
exports.icon = 'random';
exports.npm = ['crypto'];

exports.html = `<div class="padding">
	<div data-jc="dropdown" data-jc-path="hash" class="m" data-jc-config="items:,md5;required:true">@(Hash)</div>
</div>`;

exports.readme = `# Crypto Hash

`;

exports.install = function(instance) {
    instance.custom.reconfigure = function () {
        if (instance.options.hash) {
            return instance.status('Configured', 'white');
        }

        instance.status('Not configured', 'red');
    };
	instance.on('data', function(flowdata) {
		if (!flowdata.data)
            return;
            
        var crypto = require("crypto");
        var encText = crypto.createHash(instance.options.hash).update(JSON.stringify(flowdata.data)).digest("hex");
        flowdata.data = encText;
        instance.send2(flowdata);
	});

	instance.reconfigure = function() {

	};

	instance.on('options', instance.custom.reconfigure);
    instance.reconfigure();
    instance.custom.reconfigure();
};