exports.id = 'elasticsearchconnectot';
exports.title = 'Elasticsearch Connector';
exports.version = '1.0.0';
exports.group = 'Databases';
exports.author = 'Leonardo Alves';
exports.color = '#D770AD';
exports.input = false;
exports.output = 0;
exports.options = { host: '127.0.0.1', port: '9200' };
exports.readme = `# Elasticsearch Connector
This component sets a data source connection to be used by other Elasticsearch components.
`;

exports.html = `
<div class="padding">
    <div data-jc="textbox" data-jc-path="host" data-jc-config="required:true" class="m mt10">Host name</div>
    <div data-jc="textbox" data-jc-path="port" data-jc-config="required:true" class="m mt10">Port</div>
    <br />
	<section>
		<label><i class="fa fa-lock"></i>@(Elasticsearch authentication)</label>
		<div class="padding npb">
			<div class="row">
				<div class="col-md-6 m">
					<div data-jc="textbox" data-jc-path="username">@(User)</div>
				</div>
				<div class="col-md-6 m">
					<div data-jc="textbox" data-jc-path="userpassword" data-jc-config="type:password">@(Password)</div>
				</div>
			</div>
		</div>
	</section>
	<br />
</div>`;

var Clients = [];
global.ELASTICSEARCH = {};

exports.install = function (instance) {

    var dbConnection;

    instance.custom.reconfigure = function (o, old_options) {
        var options = instance.options;

        if (!options.host || !options.port) {
            instance.status('Not configured', 'red');
            return;
        }

        options.id = `${(options.username || '')}@${options.host}:${options.port}`;

        if (dbConnection) {
            JSON.stringify(options) !== JSON.stringify(old_options) && dbConnection.close();
            EMIT('elasticsearch.connections.status', 'reconfigured', old_options.id, options.id);
        }

        instance.custom.createConnection();
    };

    instance.custom.createConnection = function () {

        ON('elasticsearch.connections.status', connectionStatus);

        var o = instance.options;
        var opts = { 
            id: o.id,
            dbConnection: `//${o.host}:${o.port}`,
            username: o.username,
            userpassword: o.username,
        };

        Clients.push(opts);
        instance.status('Ready', 'white');
    };

    function connectionStatus(status, connectionId, err) {
        if (connectionId !== instance.options.id)
            return;

        switch (status) {
            case 'connecting':
                instance.status('Connecting', '#a6c3ff');
                break;
            case 'connected':
                instance.status('Connected', 'green');
                break;
            case 'disconnected':
                instance.status('Disconnected', 'red');
                break;
            case 'connectionfailed':
                instance.status('Connection failed', 'red');
                break;
            case 'error':
                instance.error('Elasticsearch Error, ID: ' + instance.id + '\n  ' + err);
                break;
        }
    }

    instance.on('options', instance.custom.reconfigure);
    instance.custom.reconfigure();
}

FLOW.trigger('elasticsearch.connections', function (next) {
    var connections = [''];
    Clients.forEach(n => connections.push(n.id));
    next(connections);
});


ELASTICSEARCH.connector = function (connectionId) {
    return Clients.findItem('id', connectionId);
};


ELASTICSEARCH.connect = function (connectionId) {
    var connection = ELASTICSEARCH.connector(connectionId);
    if (connection) {
        return connection.dbConnection;
    } else {
        EMIT('elasticsearch.connections.status', 'error', connectionId, 'No such connector');
    }
};
