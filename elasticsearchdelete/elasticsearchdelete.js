exports.id = 'elasticsearchdelete';
exports.title = 'Elasticsearch Delete';
exports.version = '1.0.0';
exports.group = 'Databases';
exports.author = 'Leonardo Alves';
exports.color = '#D770AD';
exports.input = true;
exports.output = 1;
exports.npm = ['elasticsearch'];
exports.options = {};
exports.readme = `# Elasticsearch Delete
# Input
- This component will Delete a document by ID.
- Expects data to be an Object with \`document ID\` and \`clientid\` properties:
\`\`\`javascript
{ 
    clientid: String,
    id: String
}
\`\`\`

# Output
- Output is the result received form Elasticsearch.
`;

exports.html = `
<div class="padding">
    <div data-jc="dropdown" data-jc-path="connector" data-jc-config="datasource:elasticsearchConfig.connections;required:true" class="m">@(Connectors)</div>
    <div data-jc="textbox" data-jc-path="index" data-jc-config="required:true" class="m mt10">Index</div>
    <div data-jc="textbox" data-jc-path="type" data-jc-config="required:true" class="m mt10">Type</div>
</div>
<script>
	var elasticsearchConfig = { connections: [] };
	ON('open.elasticsearchdelete', function(component, options) {
		TRIGGER('elasticsearch.connections', 'elasticsearchConfig.connections');
	});
	ON('save.elasticsearchdelete', function(component, options) {
	!component.name && (component.name = 'Delete-' + options.connector);
	});
</script>`;

exports.install = function (instance) {

    instance.custom.reconfigure = function () {

        if (!instance.options.connector)
            return instance.status('No connector', 'red');

        if (instance.options.connector) {
            return instance.status('Configured', 'white');

        }

        instance.status('Not configured', 'red');
    };

    instance.on('options', instance.custom.reconfigure);

    instance.on('data', function (flowdata) {

        try {
            var elasticsearch = require('elasticsearch');
            var options = instance.options;

            if (!instance.options.connector) {
                return instance.error(new Error('No connector'));
            } else {
                instance.status('Configured', 'white');
            }

            var db = ELASTICSEARCH.connector(instance.options.connector);
            var client = new elasticsearch.Client({
                host: db.dbConnection,
                log: 'trace'
            });
            var index = `${flowdata.data.clientid}-${options.index}`;
            var settings = {
                index: index,
                type: options.type,
                body: {
                    query: {
                        term: flowdata.data.query
                    }
                }
            };
            client.deleteByQuery( settings , function (error, response) {
                if(error){
                    instance.error(`Error ${error.statusCode} - ${error.message}`);
                    return;
                }
                flowdata.data = response.result;
                instance.send2(flowdata);
              });
            
        } catch (error) {
            console.log(error);
            instance.error(error);
        }

    });

    instance.on('close', function () {
        OFF('elasticsearch.connections.status', connectorStatus);
    });

    ON('elasticsearch.connections.status', connectorStatus);

    function connectorStatus(status, connectorId, msg) {
        if (connectorId !== instance.options.connector)
            return;

        switch (status) {
            case 'connecting':
                instance.status('Connecting', '#a6c3ff');
                break;
            case 'connected':
                instance.status('Connected', 'green');
                break;
            case 'disconnected':
                instance.status('Disconnected', 'red');
                break;
            case 'connectionfailed':
                instance.status('Connection failed', 'red');
                break;
            case 'new':
            case 'removed':
                instance.custom.reconfigure();
                break;
            case 'error':
                instance.status(msg, 'red');
                break;
            case 'reconfigured':
                instance.options.connector = msg;
                instance.reconfig();
                instance.custom.reconfigure();
                break;
        }
    }

    instance.custom.reconfigure();
}