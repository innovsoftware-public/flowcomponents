exports.id = 'elasticsearchsearch';
exports.title = 'Elasticsearch Search';
exports.version = '1.0.0';
exports.group = 'Databases';
exports.author = 'Leonardo Alves';
exports.color = '#D770AD';
exports.input = true;
exports.output = 1;
exports.npm = ['elasticsearch'];
exports.options = {};
exports.readme = `# Elasticsearch Search
This component returns all documents in the Index
- Expects data to be an Object with \`clientid\` property:
\`\`\`javascript
{ 
    clientid: String
}
\`\`\`
`;

exports.html = `
<div class="padding">
    <div data-jc="dropdown" data-jc-path="connector" data-jc-config="datasource:elasticsearchConfig.connections;required:true" class="m">@(Connectors)</div>
    <div data-jc="textbox" data-jc-path="index" data-jc-config="required:true" class="m mt10">Index</div>
    <div data-jc="textbox" data-jc-path="type" data-jc-config="required:false" class="m mt10">Type</div>
</div>
<script>
	var elasticsearchConfig = { connections: [] };
	ON('open.elasticsearchsearch', function(component, options) {
		TRIGGER('elasticsearch.connections', 'elasticsearchConfig.connections');
	});
	ON('save.elasticsearchsearch', function(component, options) {
	!component.name && (component.name = 'Search-' + options.connector);
	});
</script>`;

exports.install = function (instance) {

    instance.custom.reconfigure = function () {

        if (!instance.options.connector)
            return instance.status('No connector', 'red');

        if (instance.options.connector) {
            return instance.status('Configured', 'white');

        }

        instance.status('Not configured', 'red');
    };

    instance.on('options', instance.custom.reconfigure);

    instance.on('data', function (flowdata) {

        try {
            var elasticsearch = require('elasticsearch');
            var options = instance.options;

            if (!instance.options.connector) {
                return instance.error(new Error('No connector'));
            } else {
                instance.status('Configured', 'white');
            }

            var db = ELASTICSEARCH.connector(instance.options.connector);
            var client = new elasticsearch.Client({
                host: db.dbConnection,
                log: 'trace'
            });

            var index = `${flowdata.data.clientid}-${options.index}`;
            var query = "";
            if(flowdata.data.query){
                query = flowdata.data.query;
            }
            var type = options.type;
            client.search({
                index: index,
                type: type,
                q: query
              }, function (error, response) {
                if(error){
                    console.log(`Error ${error.statusCode} - ${error.message}`);
                    instance.error(`Error ${error.statusCode} - ${error.message}`);
                    return;
                }
                flowdata.data = response.hits.hits
                instance.send2(flowdata);
              });
        } catch (error) {
            console.log(error);
            instance.error(error);
        }

    });

    instance.on('close', function () {
        OFF('elasticsearch.connections.status', connectorStatus);
    });

    ON('elasticsearch.connections.status', connectorStatus);

    function connectorStatus(status, connectorId, msg) {
        if (connectorId !== instance.options.connector)
            return;

        switch (status) {
            case 'connecting':
                instance.status('Connecting', '#a6c3ff');
                break;
            case 'connected':
                instance.status('Connected', 'green');
                break;
            case 'disconnected':
                instance.status('Disconnected', 'red');
                break;
            case 'connectionfailed':
                instance.status('Connection failed', 'red');
                break;
            case 'new':
            case 'removed':
                instance.custom.reconfigure();
                break;
            case 'error':
                instance.status(msg, 'red');
                break;
            case 'reconfigured':
                instance.options.connector = msg;
                instance.reconfig();
                instance.custom.reconfigure();
                break;
        }
    }

    instance.custom.reconfigure();
}