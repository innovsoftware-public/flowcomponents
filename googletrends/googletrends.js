exports.id = 'googletrends';
exports.title = 'Google Trends';
exports.group = 'Google Services';
exports.version = '1.0.0';
exports.author = 'Leonardo Alves';
exports.color = '#ff4300';
exports.click = false;
exports.input = true;
exports.output = true;
exports.npm = ['google-trends-api', 'crypto-js'];
exports.readme = `# Google Trends

This component provides an API layer to google trends data.

### Input
Expects an object with \`API Method\` and \`Parameters Object\`

\`API Method\` is a String with one of the following options: "autoComplete", "interestbyregion", "interestovertime", "relatedqueries" or "relatedtopics".

\`Parameters Object\` is an object with the following options keys:

* __keyword__ Target search term(s) string or array if you wish to compare search terms required
* __startTime__ Start of time period of interest (new Date() object). If startTime is not provided, a date of January 1, 2004 is assumed (this is the oldest available google trends data)
* __endTime__ End of time period of interest (new Date() object). If endTime is not provided, the current date is selected.
* __geo__ Location of interest (string).
* __hl__ Preferred language (string defaults to english)
* __timezone__ Timezone (number defaults to the time zone difference, in minutes, from UTC to current locale (host system settings))
* __category__ Category to search within (number defaults to all categories)
* __property__ Google property to filter on. Defaults to web search. (enumerated string ['images', 'news', 'youtube' or 'froogle'] where froogle is Google Shopping results)
* __resolution__ Granularity of the geo search (enumerated string ['COUNTRY', 'REGION', 'CITY', 'DMA']). resolution is specific to the interestByRegion method.
* __granularTimeResolution__ Boolean that dictates if the results should be given in a finer time resolution (if startTime and endTime is less than one day, this should be set to true)


### Output
`;

exports.html = `<div class="padding">
	<div class="row">
		<div class="col-md-12">
            <div data-jc="dropdown" data-jc-path="method" data-jc-config="items:autoComplete,interestByRegion,interestOverTime,relatedQueries,relatedTopics;required:true" class="m">@(API Method)</div>
            <!-- <div data-jc="dropdowncheckbox" data-jc-path="selected" data-jc-config="required:true;items:autoComplete,interestByRegion,interestOverTime,relatedQueries,relatedTopics;visible:4">@(API Method)</div> -->
			<div data-jc="textbox" data-jc-path="params" data-jc-config="" class="m">@(Parameters Object)</div>
            <div data-jc="checkbox" data-jc-path="flowdataRequired">Use flowdata</div>
            <div class="help m">@(If checked you need to receive an Object with API method and Parameters object e.g. {"method": "interestOverTime", "params": {"keyword": "Valentines Day"}}.)</div>
		</div>
	</div>
</div>`;

exports.install = function(instance) {
    const googleTrends = require('google-trends-api');
    var crypto = require("crypto");
    var CryptoJS = require("crypto-js");
    
    instance.custom.reconfigure = function () {
        if (!instance.options.method && instance.options.flowdataRequired) {
            //return instance.status('Configured', 'white');
            instance.status('Not configured', 'red');
        }
        instance.status('Configured', 'white');
        //instance.status('Not configured', 'red');
    };
	instance.on('data', function(flowdata) {
        var options = instance.options;
        var data = {};
		if(options.flowdataRequired){
            data['method'] = flowdata.data.method;
            data['params'] = flowdata.data.params;
        }else {
            data['method'] = options.method;
            data['params'] = JSON.parse(options.params);            
        }

        // //var cipher = crypto.createCipher('aes-256-ecb', '');
        // var cipher = crypto.createCipher('aes-128-cbc', '');
        // encText = cipher.update(JSON.stringify(data.params), 'utf8', 'hex') + cipher.final('hex');
        // //encText = "dcbc00077074d779b78584e76569f43f8a98ada1e2639df13fed2687176afd5d57732d567ba068c01efd4a6484189b332ea018525011cce8cc90015bc1a686a47e378d6d0acda83c5d09c4437d1aacce5dafcdcdb6e1f7f9071638ac9fb15303d19eb2748349beaf7aa2765825965082c06eb9ee46f8115abea0b5f22f44239ba";
        // console.log(encText);
        var dataToParse = {
            "keyword": data.params.keyword,
            "startTime": data.params.startTime,
            "endTime": data.params.endTime
        }
        
        var encText = crypto.createHash('md5').update(JSON.stringify(dataToParse)).digest("hex");
        
        //var encText = crypto.createHash('md5').update(JSON.stringify(data.params)).digest("hex");
        // var cipher = crypto.createDecipher('aes-256-ecb', '');
        // dencText = cipher.update('dcbc00077074d779b78584e76569f43f8a98ada1e2639df13fed2687176afd5d57732d567ba068c01efd4a6484189b332ea018525011cce8cc90015bc1a686a47e378d6d0acda83c5d09c4437d1aacce5dafcdcdb6e1f7f9071638ac9fb15303d19eb2748349beaf7aa2765825965082c06eb9ee46f8115abea0b5f22f44239b', 'hex', 'utf8');
        // console.log('dencText: ' + dencText);
        var resultado = {};
        if(Array.isArray(data.method)){
            Object.keys(data.method).forEach(function(key){
                getResult(data.method[key], encText, data.params)
                .then((val)=>{
                    resultado[data.method[key]] = val;
                    if(data.method.length-1 == key){
                        flowdata.data = resultado;
                        instance.send2(flowdata);
                    }
                })
                .catch((err)=>{
                    instance.error(err);
                });
            });

        }
        getResult(data.method, encText, data.params)
        .then((val)=>{
            resultado[data.method] = val;
            instance.send2(resultado);
        })
        .catch((err)=>{
            instance.error(JSON.stringify(err));
        });
        // switch (data.method) {
        //     case "autoComplete":
        //         autoComplete(flowdata, encText, data.params);
        //         break;
        //     case "interestOverTime":
        //         interestOverTime(flowdata, encText, data.params);
        //         break;
        //     case "interestByRegion":
        //         interestByRegion(flowdata, encText, data.params);
        //         break;
        //     case "relatedQueries":
        //         relatedQueries(flowdata, encText, data.params);
        //         break;
        //     case "relatedTopics":
        //         relatedTopics(flowdata, encText, data.params);
        //         break;                          
        //     default:
        //         break;
        // }
	});

	instance.on('options', function() {
		instance.custom.status();
	});

	instance.custom.status = function() {
		
	};

    instance.custom.status();
   
    function getResult(method, encText, params){
        return new Promise((resolve, reject)=>{
            var retorno;
            switch (method) {
                case "autoComplete":
                    autoComplete(encText, params)
                    .then((data)=>{
                        resolve(data);
                    })
                    .catch((err)=>{
                        reject(err);
                    });
                case "interestOverTime":
                    interestOverTime(encText, params)
                    .then((data)=>{
                        resolve(data);
                    })
                    .catch((err)=>{
                        reject(err);
                    });
                case "interestByRegion":
                    retorno = interestByRegion(encText, params)
                    .then((data)=>{
                        resolve(data);
                    })
                    .catch((err)=>{
                        reject(err);
                    });
                case "relatedQueries":
                    retorno = relatedQueries(encText, params)
                    .then((data)=>{
                        resolve(data);
                    })
                    .catch((err)=>{
                        reject(err);
                    });
                case "relatedTopics":
                    retorno = relatedTopics(encText, params)
                    .then((data)=>{
                        resolve(data);
                    })
                    .catch((err)=>{
                        reject(err);
                    });                          
                default:
                    break;
            }
        });
    }
    function autoComplete(queryId, parameters){
        return new Promise((resolve, reject)=>{
            googleTrends.autoComplete(parameters)
            .then((response) => {
                var resposta = { 
                    "queryId": queryId,
                    "params": parameters,
                    "response": JSON.parse(response).default
                };
                //console.log(resposta.default);
                resolve(resposta);
            })
            .catch((err) => {
                console.log('got the error', err);
                console.log('error message', err.message);
                console.log('request body',  err.requestBody);
                reject({ "error": err });
            });
        });
    }
    function interestOverTime(queryId, parameters){
        return new Promise((resolve, reject)=>{
            googleTrends.interestOverTime(parameters)
            .then((response) => {
                var resposta = { 
                    "queryId": queryId,
                    "params": parameters,
                    "response": JSON.parse(response).default
                };
                //console.log(resposta.default);
                resolve(resposta);
            })
            .catch((err) => {
                console.log('got the error', err);
                console.log('error message', err.message);
                console.log('request body',  err.requestBody);
                reject({ "error": err });
            });
        });
    };
    function interestByRegion(queryId, parameters){
        return new Promise((resolve, reject)=>{
            googleTrends.interestByRegion(parameters)
            .then((response) => {
                var resposta = { 
                    "queryId": queryId,
                    "params": parameters,
                    "response": JSON.parse(response).default
                };
                //console.log(resposta.default);
                resolve(resposta);
            })
            .catch((err) => {
                console.log('got the error', err);
                console.log('error message', err.message);
                console.log('request body',  err.requestBody);
                reject({ "error": err });
            });
        });
    };
    function relatedQueries(queryId, parameters){
        return new Promise((resolve, reject)=>{
            googleTrends.relatedQueries(parameters)
            .then((response) => {
                var resposta = { 
                    "queryId": queryId,
                    "params": parameters,
                    "response": JSON.parse(response).default
                };
                //console.log(resposta.default);
                resolve(resposta);
            })
            .catch((err) => {            
                console.log(err);
                reject({ "error": err });
            });
        });
    };
    function relatedTopics(queryId, parameters){
        return new Promise((resolve, reject)=>{
            googleTrends.relatedTopics(parameters)
            .then((response) => {
                var resposta = { 
                    "queryId": queryId,
                    "params": parameters,
                    "response": JSON.parse(response).default
                };
                //console.log(resposta.default);
                resolve(resposta);
            })
            .catch((err) => {
                console.log(err);
                reject({ "error": err });
            });
        });
    };
    // function autoComplete(flowdata, queryId, parameters){
    //     googleTrends.autoComplete(parameters)
    //     .then((response) => {
    //         var resposta = { 
    //             "queryId": queryId,
    //             "params": parameters,
    //             "response": JSON.parse(response).default
    //         };
    //         //console.log(resposta.default);
    //         flowdata.data = resposta;
    //         instance.send2(flowdata);
    //     })
    //     .catch((err) => {
    //         console.log('got the error', err);
    //         console.log('error message', err.message);
    //         console.log('request body',  err.requestBody);
    //     });
    // }
    // function interestOverTime(flowdata, queryId, parameters){
    //     googleTrends.interestOverTime(parameters)
    //     .then((response) => {
    //         var resposta = { 
    //             "queryId": queryId,
    //             "params": parameters,
    //             "response": JSON.parse(response).default
    //         };
    //         //console.log(resposta.default);
    //         flowdata.data = resposta;
    //         instance.send2(flowdata);
    //     })
    //     .catch((err) => {
    //         console.log('got the error', err);
    //         console.log('error message', err.message);
    //         console.log('request body',  err.requestBody);
    //     });
    // };
    // function interestByRegion(flowdata, queryId, parameters){
    //     googleTrends.interestByRegion(parameters)
    //     .then((response) => {
    //         var resposta = { 
    //             "queryId": queryId,
    //             "params": parameters,
    //             "response": JSON.parse(response).default
    //         };
    //         //console.log(resposta.default);
    //         flowdata.data = resposta;
    //         instance.send2(flowdata);
    //     })
    //     .catch((err) => {
    //         console.log(err);
    //     })
    // };
    // function relatedQueries(flowdata, queryId, parameters){
    //     googleTrends.relatedQueries(parameters)
    //     .then((response) => {
    //         var resposta = { 
    //             "queryId": queryId,
    //             "params": parameters,
    //             "response": JSON.parse(response).default
    //         };
    //         //console.log(resposta.default);
    //         flowdata.data = resposta;
    //         instance.send2(flowdata);
    //     })
    //     .catch((err) => {
    //         console.log(err);
    //     })
    // };
    // function relatedTopics(flowdata, queryId, parameters){
    //     googleTrends.relatedTopics(parameters)
    //     .then((response) => {
    //         var resposta = { 
    //             "queryId": queryId,
    //             "params": parameters,
    //             "response": JSON.parse(response).default
    //         };
    //         //console.log(resposta.default);
    //         flowdata.data = resposta;
    //         instance.send2(flowdata);
    //     })
    //     .catch((err) => {
    //         console.log(err);
    //     })
    // };
};