
exports.id = 'httprequestinnov';
exports.title = 'HTTP Request INNOV';
exports.group = 'HTTP';
exports.color = '#5D9CEC';
exports.input = true;
exports.version = '1.0.0';
exports.output = 1;
exports.author = 'Diego Amaral';
exports.icon = 'cloud-upload';
exports.modelOrigem = 'httprequest_V2.0.0';
exports.options = { headers: [{ key: '', value: '' }] };


exports.html = `<div class="padding">
	<div data-jc="textbox" data-jc-path="url" class="m" data-jc-config="required:true;maxlength:500;placeholder:@(E.g. https\\://www.totaljs.com)">@(URL address)</div>
	<div class="row">
		<div class="col-md-6 m">
			<div data-jc="dropdown" data-jc-path="method" data-jc-config="required:true;items:,GET,POST,PUT,DELETE">@(HTTP method)</div>
		</div>
		<div class="col-md-6 m">
			<div data-jc="dropdown" data-jc-path="stringify" data-jc-config="required:true;items:,URL encoded|encoded,JSON|json,RAW|raw,None|none">@(Serialization)</div>
		</div>
	</div>
	<div data-jc="checkbox" data-jc-path="chunks">@(Download the content <b>in chunks</b>)</div>
	<div data-jc="checkbox" data-jc-path="persistentcookies">@(Keep persistent cookies)</div>
	<div data-jc="checkbox" data-jc-path="nodns">@(Disable DNS cache)</div>
</div>
<hr class="nmt nmb" />
<div class="padding">
	<div data-jc="repeater" data-jc-path="headers" class="mt10">
		<script type="text/html">
		<div class="row">
			<div class="col-md-12">
				<div class="cond-col4 pr10">
					<div data-jc="textbox" data-jc-path="headers[$index].key" data-jc-config="placeholder:@(enter key)"></div>
				</div>
				<div class="cond-col4 pr10">
					<div data-jc="textbox" data-jc-path="headers[$index].value" data-jc-config="placeholder:@(enter value)"></div>
				</div>
				<div class="cond-col4">
					<button class="exec button button-small cond-remove" data-exec="#httprequestinnovcomponent_remove_header" data-index="$index"><i class="fa fa-trash"></i></button>
				</div>
			</div>
		</div>
		</script>
	</div>
	<div class="row">
		<div class="col-md-2 m">
			<br>
			<button class="exec button button-small" data-exec="#httprequestinnovcomponent_add_header"><i class="fa fa-plus mr5"></i>ADD</button>
		</div>
	</div>
			
	<div data-jc="keyvalue" data-jc-path="cookies" data-jc-config="placeholderkey:@(Cookie name);placeholdervalue:@(Cookie value and press enter)">@(Cookies)</div>
</div>
<div class="padding bg-smoke">
	<section>
		<label><i class="fa fa-lock"></i>@(HTTP basic access authentication)</label>
		<div class="padding npb">
			<div class="row">
				<div class="col-md-6 m">
					<div data-jc="textbox" data-jc-path="username">@(User)</div>
				</div>
				<div class="col-md-6 m">
					<div data-jc="textbox" data-jc-path="userpassword">@(Password)</div>
				</div>
			</div>
		</div>
	</section>


<script>

	var changed = false;
	var outputs_count;

	ON('open.httprequestinnov', function(component, options) {
		console.log('open',options);
		outputs_count = options.headers.length || 0;
	});

	ON('save.httprequestinnov', function(component, options) {
		console.log('save',options);
		var length = options.headers.length || 0;
		if (changed && length !==  outputs_count) {
			component.connections = {};
			component.output = length;
			setState(MESSAGES.apply);
		}
	});

	OPERATION('httprequestinnovcomponent_add_header', function(){
		
		PUSH('settings.httprequestinnov.headers', {key: '', value: ''});
		changed = true;
		console.log(settings.httprequestinnov.headers);
	});

	OPERATION('httprequestinnovcomponent_remove_header', function(button){
		var index = button.attr('data-index');
		var headers = settings.httprequestinnov.headers;
		headers = headers.remove('index', parseInt(index));
		SET('settings.httprequestinnov.headers', headers);
		changed = true;
	});

</script></div>`;



exports.readme = `# Request

This component creates a request with received data.

__Response:__
\`\`\`javascript
{
	data: String,
	headers: Object,
	status: Number,
	host: String
}
\`\`\`

__Dynamic arguments__:
Are performed via FlowData repository and can be used for URL address or for custom headers/cookies/auth. Use \`repository\` component for creating of dynamic arguments. Dynamic values are replaced in the form \`{key}\`:

- url address e.g. \`https://.../{key}/\`
- headers values e.g. \`{token}\`
- cookies values e.g. \`{token}\``;

exports.install = function (instance) {

	var can = false;
	var flags = null;
	var cookies2 = null;

	instance.on('data', function (response) {
		can && instance.custom.send(response);
	});

	instance.custom.send = function (response) {
		var options = instance.options;

		var headers = null;
		var cookies = null;

		// options.headers && Object.keys(options.headers).forEach(function (key) {
		// 	!headers && (headers = {});
		// 	headers[key] = response.arg(options.headers[key]);
		// });

		if (options.headers) {
			options.headers.forEach(function (item) {
				!headers && (headers = {});
				if (item.key) {
					headers[item.key] = item.value;
				}
			});
		}

	//	options.headers = headers;

		if (options.username && options.userpassword) {
			!headers && (headers = {});
			headers['Authorization'] = 'Basic ' + U.createBuffer(response.arg(options.username + ':' + options.userpassword)).toString('base64');
		}

		options.cookies && Object.keys(options.cookies).forEach(function (key) {
			!cookies && (cookies = {});
			cookies[key] = response.arg(options.cookies[key]);
		});

		if (options.chunks) {
			U.download(response.arg(options.url), flags, options.stringify === 'none' ? null : response.data, function (err, response) {
				response.on('data', (chunks) => instance.send2(chunks));
			}, cookies || cookies2, headers);
		} else {

			//console.log(response.arg(options.url), flags, options.stringify === 'none' ? null : response.data);
			U.request(response.arg(options.url), flags, options.stringify === 'none' ? null : response.data, function (err, data, status, headers, host) {
				if (response && !err) {
					response.data = { data: data, status: status, headers: headers, host: host };
					instance.send2(response);
				} else if (err)
					instance.error(err, response);
			}, cookies || cookies2, headers);
		}
	};

	instance.reconfigure = function () {
		var options = instance.options;
		can = options.url && options.url && options.method && options.stringify ? true : false;
		instance.status(can ? '' : 'Not configured', can ? undefined : 'red');

		if (!can)
			return;

		flags = [];
		flags.push(options.method.toLowerCase());
		options.stringify === 'json' && flags.push('json');
		options.stringify === 'raw' && flags.push('raw');
		!options.nodns && flags.push('dnscache');
		if (options.persistentcookies) {
			flags.push('cookies');
			cookies2 = {};
		} else
			cookies2 = null;
	};

	instance.on('options', instance.reconfigure);
	instance.reconfigure();
};