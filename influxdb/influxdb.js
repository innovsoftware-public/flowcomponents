exports.id = 'influxdb';
exports.title = 'InfluxDB';
exports.version = '1.0.0';
exports.group = 'Databases';
exports.author = 'Leonardo Alves';
exports.color = '#D770AD';
exports.input = true;
exports.output = 1;
exports.options = {};
exports.npm = ['influxdb-nodejs'];
exports.readme = `# InfluxDB

## Outputs

## Query

## Write`;

exports.html = `
<div class="padding">
    <div data-jc="textbox" data-jc-path="host" data-jc-config="required:true" class="m mt10">Host name</div>
    <div data-jc="textbox" data-jc-path="port" data-jc-config="required:true" class="m mt10">Port</div>
    <div data-jc="textbox" data-jc-path="database" data-jc-config="required:true" class="m mt10">DB name</div>
	<div data-jc="textbox" data-jc-path="measurement" data-jc-config="required:true" class="m mt10">Measurement name</div>
	<div data-jc="dropdown" data-jc-path="method" data-jc-config="required:true;items: query,write" class="m">@(Method)</div>
	<div data-jc="visible" data-jc-path="method" data-jc-config="if:value === 'insert'">
		<div data-jc="checkbox" data-jc-path="addid">Add unique ID to data before insert</div>
    </div>
    <br />
	<section>
		<label><i class="fa fa-lock"></i>@(InfluxDB authentication)</label>
		<div class="padding npb">
			<div class="row">
				<div class="col-md-6 m">
					<div data-jc="textbox" data-jc-path="username">@(User)</div>
				</div>
				<div class="col-md-6 m">
					<div data-jc="textbox" data-jc-path="userpassword" data-jc-config="type:password">@(Password)</div>
				</div>
			</div>
		</div>
	</section>
	<br />
</div>`;

exports.install = function(instance) {
    instance.on('data', function(flowdata) {
        var options = instance.options;
        var Influx = require('influxdb-nodejs');

        var url;
        if(options.username){
            var url = `mongodb://${options.username}:${options.userpassword}@${options.host}:${options.port}/${options.database}`;
        }else {
            var url = `mongodb://${options.host}:${options.port}/${options.database}`;
        }

        // const client = new Influx('http://192.168.1.197:8086/mydb');
        const client = new Influx(`${options.host}:${options.port}/${options.database}`);

        if (options.method === 'query') {
            client.queryRaw(`select * from "${options.measurement}"`)
                .then(function(info){
                    flowdata.data = info;
                    instance.send2(0, flowdata);
                })
                .catch(function(error){
                    flowdata.data = error;
                    instance.send2(0, flowdata);
                });
        } else if (options.method === 'write') {
            if(Array.isArray(flowdata.data)){
                for(var t=0; t < flowdata.data.length; t++){
                    client.write(options.measurement)
                    .tag(flowdata.data[t].tag)
                    .field(flowdata.data[t].fields)
                    .time(flowdata.data[t].time, "ms")
                    .then(function(info){
                    })
                    .catch(function(error){
                        flowdata.data = error;
                        instance.send2(0, flowdata);
                    });
                }
                flowdata.data = 'Success';
                instance.send2(0, flowdata);
            }else{
                client.write(options.measurement)
                .tag(flowdata.data.tag)
                .field(flowdata.data.fields)
                .time(flowdata.data.time, "ms")
                .then(function(info){
                    flowdata.data = 'Success';
                    instance.send2(flowdata);
                })
                .catch(function(error){
                    flowdata.data = error;
                    instance.send2(flowdata);
                });
            }

        }
        // .then(console.info)
        // .catch(console.error);
        // instance.send2(1,flowdata);
    });
}