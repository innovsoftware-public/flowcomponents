exports.id = 'mongodb';
exports.title = 'MongoDB';
exports.version = '1.0.0';
exports.group = 'Databases';
exports.author = 'Leonardo Alves';
exports.color = '#D770AD';
exports.input = true;
exports.output = 2;
exports.options = {};
exports.readme = `# MongoDB

## Outputs
First output is response from nosql engine and second is the data passed in

## Delete
will delete a document by id

## Find
will find a document
- Filter:
\`\`\`
 { 'name': 'Leonardo'}
\`\`\`

## Insert
- will insert recieved data
- expects data to be an Object
- returns error, success, id

## Update
- will update document by id
- expects data to be an Object with \`id\` property and an Object with data to update
\`\`\`
{ "id": "99408904rorio", "data": { "field1": "value1", "field2": "value2"}}
\`\`\``;

exports.html = `
<div class="padding">
    <div data-jc="textbox" data-jc-path="host" data-jc-config="required:true" class="m mt10">Host name</div>
    <div data-jc="textbox" data-jc-path="port" data-jc-config="required:true" class="m mt10">Port</div>
    <div data-jc="textbox" data-jc-path="database" data-jc-config="required:true" class="m mt10">DB name</div>
	<div data-jc="textbox" data-jc-path="collection" data-jc-config="required:true" class="m mt10">DB collection name</div>
	<div data-jc="dropdown" data-jc-path="method" data-jc-config="required:true;items: delete, find, findOne, insert, update" class="m">@(Method)</div>
	<div data-jc="visible" data-jc-path="method" data-jc-config="if:value === 'insert'">
		<div data-jc="checkbox" data-jc-path="addid">Add unique ID to data before insert</div>
    </div>
    <br />
	<section>
		<label><i class="fa fa-lock"></i>@(MongoDB authentication)</label>
		<div class="padding npb">
			<div class="row">
				<div class="col-md-6 m">
					<div data-jc="textbox" data-jc-path="username">@(User)</div>
				</div>
				<div class="col-md-6 m">
					<div data-jc="textbox" data-jc-path="userpassword" data-jc-config="type:password">@(Password)</div>
				</div>
			</div>
		</div>
	</section>
	<br />
</div>`;

exports.install = function(instance) {
    instance.on('data', function(flowdata) {
        var options = instance.options;
        var MongoClient = require('mongodb').MongoClient;
        var Object_Id = require('mongoose').Types.ObjectId;

        var url;
        if(options.username){
            var url = `mongodb://${options.username}:${options.userpassword}@${options.host}:${options.port}/${options.database}`;
        }else {
            var url = `mongodb://${options.host}:${options.port}/${options.database}`;
        }

        MongoClient.connect(url, function(err, database) {
            if (err) throw err;
            const base = database.db(options.database);
            
            if (options.method === 'delete') {
                base.collection(options.collection).deleteOne({ "_id": new Object_Id(flowdata.data.id)}, function(err, result){
                    if (err) return instance.error(err);
                    flowdata.data = result;
                    instance.send2(0, flowdata);

                    database.close();
                })
            } else if (options.method === 'find') {
                var query = {};
                if(flowdata.data && flowdata.data.where){
                    query = flowdata.data.where;
                }
                var order = {};
                if(flowdata.data && flowdata.data.order){
                    order = flowdata.data.order;
                }

                base.collection(options.collection).find(query).sort(order).toArray(function(err, result) {
                    if (err) return instance.error(err);
                    flowdata.data = result;
                    instance.send2(0, flowdata);
                    
                    database.close();
                });
            } else if (options.method === 'findOne') {
                var query = {};
                
                if(flowdata.data.where){
                    query = flowdata.data.where;
                }
                var order = {};
                if(flowdata.data.order){
                    order[flowdata.data.order] = -1;
                }
                base.collection(options.collection).find(query).sort(order).limit(1).toArray(function(err, result) {
                    if (err) return instance.error(err);
                    flowdata.data = result;
                    instance.send2(0, flowdata);
                    
                    database.close();
                });
            } else if (options.method === 'insert') {
                base.collection(options.collection).insert(flowdata.data, function(err, result){
                    if (err) return instance.error(err);
                    flowdata.data = { insetedIds: result.insertedIds };
                    instance.send2(0, flowdata);
                    
                    database.close();
                })
            } else if (options.method === 'update') {
                base.collection(options.collection).update({ "_id": new Object_Id(flowdata.data.id) }, flowdata.data.data, function(err, result){
                    if (err) return instance.error(err);
                    
                    if(result.nModified == 1){
                        flowdata.data = 'Update Success';
                        instance.send2(0, flowdata);
                    }else{
                        flowdata.data = result;
                        instance.send2(0, flowdata);
                    }

                    database.close();
                })
            }
        });
        instance.send2(1,flowdata);
    });
}