exports.id = 'mongodbconnector';
exports.title = 'MongoDB Connector';
exports.version = '1.0.1';
exports.group = 'Databases';
exports.author = 'Diego Amaral';
exports.color = '#D770AD';
exports.input = false;
exports.output = 0;
exports.options = { host: '127.0.0.1', port: '27017' };
exports.readme = `# MongoDB Connector`;
exports.npm = ['mongodb', 'mongoose']

exports.html = `
<div class="padding">
    <div data-jc="textbox" data-jc-path="host" data-jc-config="required:true" class="m mt10">Host name</div>
    <div data-jc="textbox" data-jc-path="port" data-jc-config="required:true" class="m mt10">Port</div>
    <div data-jc="textbox" data-jc-path="database" data-jc-config="required:true" class="m mt10">DB name</div>
    <br />
	<section>
		<label><i class="fa fa-lock"></i>@(MongoDB authentication)</label>
		<div class="padding npb">
			<div class="row">
				<div class="col-md-6 m">
					<div data-jc="textbox" data-jc-path="username">@(User)</div>
				</div>
				<div class="col-md-6 m">
					<div data-jc="textbox" data-jc-path="userpassword" data-jc-config="type:password">@(Password)</div>
				</div>
			</div>
		</div>
	</section>
	<br />
</div>`;


var MongoClients = [];

global.MONGODB = {};


exports.install = function (instance) {

    var dbConnection;

    var MongoClient = require('mongodb').MongoClient;
    var Object_Id = require('mongoose').Types.ObjectId;

    instance.custom.reconfigure = function (o, old_options) {

        if (old_options)
            MongoClients = MongoClients.remove(function (cn) {
                return cn.id === old_options.id;
            });

        var options = instance.options;

        if (!options.host || !options.port) {
            instance.status('Not configured', 'red');
            return;
        }

        options.id = `${(options.username || '')}@${options.host}:${options.port}/${options.database}`;

        if (dbConnection) {
            JSON.stringify(options) !== JSON.stringify(old_options) && dbConnection.close();
            EMIT('mongo.connections.status', 'reconfigured', old_options.id, options.id);
        }

        instance.custom.createConnection();
    };

    instance.custom.createConnection = function () {

        ON('mongo.connections.status', connectionStatus);

        var o = instance.options;
        var opts = { id: o.id };

        if (o.username) {
            var url = `mongodb://${o.username}:${o.userpassword}@${o.host}:${o.port}/${o.database}`;
        } else {
            var url = `mongodb://${o.host}:${o.port}/${o.database}`;
        }

        opts.dbConnection = url;//\new MongoClient(url);
        MongoClients.push(opts);

        instance.status('Ready', 'white');
    };


    // instance.close = function (done) {

    //     dbConnection && dbConnection.close(function () {
    //         MongoClients = MongoClients.remove('id', instance.options.id);
    //         EMIT('mongo.connections.status', 'removed', instance.options.id);
    //     });

    //     OFF('mongo.connections.status', connectionStatus);

    //     done();
    // };

    function connectionStatus(status, connectionId, err) {
        if (connectionId !== instance.options.id)
            return;

        switch (status) {
            case 'connecting':
                instance.status('Connecting', '#a6c3ff');
                break;
            case 'connected':
                instance.status('Connected', 'green');
                break;
            case 'disconnected':
                instance.status('Disconnected', 'red');
                break;
            case 'connectionfailed':
                instance.status('Connection failed', 'red');
                break;
            case 'error':
                instance.error('Mongo Error, ID: ' + instance.id + '\n  ' + err);
                break;
        }
    }

    instance.on('options', instance.custom.reconfigure);
    instance.custom.reconfigure();
}

//console.log(FLOW);
FLOW.trigger('mongo.connections', function (next) {
    var connections = [''];
    MongoClients.forEach(n => connections.push(n.id));
    next(connections);
});


MONGODB.connector = function (connectionId) {
    return MongoClients.findItem('id', connectionId);
};


MONGODB.connect = function (connectionId) {
    var connection = MONGODB.connector(connectionId);
    if (connection) {
        return connection.dbConnection;
    } else {
        EMIT('mongo.connections.status', 'error', connectionId, 'No such connector');
    }
};
