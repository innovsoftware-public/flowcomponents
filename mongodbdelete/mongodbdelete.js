exports.id = 'mongodbdelete';
exports.title = 'MongoDB Delete';
exports.version = '1.0.1';
exports.group = 'Databases';
exports.author = 'Diego Amaral';
exports.color = '#D770AD';
exports.input = true;
exports.output = 2;
exports.options = {};
exports.npm = ['mongodb', 'mongoose']
exports.readme = `# MongoDB Delete

## Outputs
First output is response from nosql engine and second is the data passed in

## Delete
will delete a document by id`;

exports.html = `
<div class="padding">
    <div data-jc="dropdown" data-jc-path="connector" data-jc-config="datasource:mongoConfig.connections;required:true" class="m">@(Connectors)</div>
    <div data-jc="textbox" data-jc-path="collection" data-jc-config="required:true" class="m mt10">DB collection name</div>
</div>
<script>
	var mongoConfig = { connections: [] };
	ON('open.mongodbdelete', function(component, options) {
		TRIGGER('mongo.connections', 'mongoConfig.connections');
	});
	ON('save.mongodbdelete', function(component, options) {
	!component.name && (component.name = options.collection +'/delete');
	});
</script>`;

exports.install = function (instance) {

    var MongoClient = require('mongodb').MongoClient;
    var Object_Id = require('mongoose').Types.ObjectId;
    instance.custom.reconfigure = function () {
        if (!instance.options.connector)
            return instance.status('No connector', 'red');

        if (instance.options.connector) {
            return instance.status('Configured', 'white');

        }

        instance.status('Not configured', 'red');
    };

    instance.on('options', instance.custom.reconfigure);

    instance.on('data', function (flowdata) {
        var options = instance.options;
        var MongoClient = MONGODB.connector(instance.options.connector);

        if (!MongoClient) {
            return instance.error(new Error('No connector'));
        }

        var db = MONGODB.connector(instance.options.connector);

        var url = db.dbConnection;

        var mongbDb = new MongoClient(url);

        mongbDb.connect(function (err, database) {
            if (err) return instance.error(err);
            const base = database.db(database.s.options.dbName);

            base.collection(options.collection).deleteOne({ "_id": new Object_Id(flowdata.data.id) }, function (err, result) {
                if (err) return instance.error(err);
                flowdata.data = result;
                instance.send2(0, flowdata);

                database.close();
            })

        });
        instance.send2(1, flowdata);
    });


    instance.on('close', function () {
     //   MONGODB.remove(instance.options.connector, instance.id);
        OFF('mongo.connections.status', connectorStatus);
    });

    ON('mongo.connections.status', connectorStatus);

    function connectorStatus(status, connectorId, msg) {
        if (connectorId !== instance.options.connector)
            return;

        switch (status) {
            case 'connecting':
                instance.status('Connecting', '#a6c3ff');
                break;
            case 'connected':
                instance.status('Connected', 'green');
                break;
            case 'disconnected':
                instance.status('Disconnected', 'red');
                break;
            case 'connectionfailed':
                instance.status('Connection failed', 'red');
                break;
            case 'new':
            case 'removed':
                instance.custom.reconfigure();
                break;
            case 'error':
                instance.status(msg, 'red');
                break;
            case 'reconfigured':
                instance.options.connector = msg;
                instance.reconfig();
                instance.custom.reconfigure();
                break;
        }
    }

    instance.custom.reconfigure();
}