exports.id = 'mongodbfind';
exports.title = 'MongoDB Find';
exports.version = '1.0.1';
exports.group = 'Databases';
exports.author = 'Diego Amaral';
exports.color = '#D770AD';
exports.input = true;
exports.output = 2;
exports.npm = ['mongodb', 'mongoose']
exports.options = {};
exports.readme = `# MongoDB Find

## Outputs
First output is response from nosql engine and second is the data passed in

## Find
will find a document
- Filter:
\`\`\`
 { 'field': 'value'}
\`\`\`

`;

exports.html = `
<div class="padding">
    <div data-jc="dropdown" data-jc-path="connector" data-jc-config="datasource:mongoConfig.connections;required:true" class="m">@(Connectors)</div>
    <div data-jc="textbox" data-jc-path="collection" data-jc-config="required:true" class="m mt10">DB collection name</div>
	<div data-jc="dropdown" data-jc-path="method" data-jc-config="required:true;items: find, findOne" class="m">@(Method)</div>
</div>
<script>
	var mongoConfig = { connections: [] };
	ON('open.mongodbfind', function(component, options) {
		TRIGGER('mongo.connections', 'mongoConfig.connections');
	});
	ON('save.mongodbfind', function(component, options) {
	!component.name && (component.name = options.collection + '/' + options.method);
	});
</script>`;

exports.install = function (instance) {

    var MongoClient = require('mongodb').MongoClient;
    var Object_Id = require('mongoose').Types.ObjectId;


    instance.custom.reconfigure = function () {

        if (!instance.options.connector)
            return instance.status('No connector', 'red');

        if (instance.options.connector) {
            return instance.status('Configured', 'white');

        }

        instance.status('Not configured', 'red');
    };

    instance.on('options', instance.custom.reconfigure);

    instance.on('data', function (flowdata) {

        try {
            var options = instance.options;
            
            if (!instance.options.connector) {
                return instance.error(new Error('No connector'));
            } else {
                instance.status('Configured', 'white');
            }

            var db = MONGODB.connector(instance.options.connector);

            var url = db.dbConnection;

            var mongbDb = new MongoClient(url);
    
            mongbDb.connect(function (err, database) {
                if (err) {
                    //   console.log(err);
                    return instance.error(err);
                }

                const base = database.db(database.s.options.dbName);

                if (options.method === 'find') {
                    var query = {};
                    if (flowdata.data && flowdata.data.where) {
                        query = flowdata.data.where;
                    }
                    var order = {};
                    if (flowdata.data && flowdata.data.order) {
                        order = flowdata.data.order;
                    }

                    base.collection(options.collection).find(query).sort(order).toArray(function (err, result) {
                        if (err) return instance.error(err);
                        flowdata.data = result;
                        instance.send2(0, flowdata);

                        database.close();
                    });
                } else if (options.method === 'findOne') {
                    var query = {};

                    if (flowdata.data && flowdata.data.where) {
                        query = flowdata.data.where;
                    }
                    var order = {};
                    if (flowdata.data && flowdata.data.order) {
                        order[flowdata.data.order] = -1;
                    }
                    base.collection(options.collection).find(query).sort(order).limit(1).toArray(function (err, result) {
                        if (err) return instance.error(err);
                        flowdata.data = result;
                        instance.send2(0, flowdata);
                        
                        database.close();
                    });
                }
            });
            instance.send2(1, flowdata);
        } catch (error) {
            console.log(error);
            instance.error(error);
        }

    });


    instance.on('close', function () {
     //   MONGODB.remove(instance.options.connector, instance.id);
        OFF('mongo.connections.status', connectorStatus);
    });

    ON('mongo.connections.status', connectorStatus);

    function connectorStatus(status, connectorId, msg) {
        if (connectorId !== instance.options.connector)
            return;

        switch (status) {
            case 'connecting':
                instance.status('Connecting', '#a6c3ff');
                break;
            case 'connected':
                instance.status('Connected', 'green');
                break;
            case 'disconnected':
                instance.status('Disconnected', 'red');
                break;
            case 'connectionfailed':
                instance.status('Connection failed', 'red');
                break;
            case 'new':
            case 'removed':
                instance.custom.reconfigure();
                break;
            case 'error':
                instance.status(msg, 'red');
                break;
            case 'reconfigured':
                instance.options.connector = msg;
                instance.reconfig();
                instance.custom.reconfigure();
                break;
        }
    }

    instance.custom.reconfigure();
}