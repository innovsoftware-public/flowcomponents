exports.id = 'mongodbsave';
exports.title = 'MongoDB Save';
exports.version = '1.0.0';
exports.group = 'Databases';
exports.author = 'Diego Amaral';
exports.color = '#D770AD';
exports.input = true;
exports.output = 2;
exports.options = {};
exports.readme = `# MongoDB Save

## Outputs
First output is response from nosql engine and second is the data passed in


## Insert
- will insert recieved data
- expects data to be an Object
- returns error, success, id

## Update
- will update document by id
- expects data to be an Object with \`id\` property and an Object with data to update
\`\`\`
{ "id": "99408904rorio", "data": { "field1": "value1", "field2": "value2"}}
\`\`\``;

exports.html = `
<div class="padding">
    <div data-jc="dropdown" data-jc-path="connector" data-jc-config="datasource:mongoConfig.connections;required:true" class="m">@(Connectors)</div>
    <div data-jc="textbox" data-jc-path="collection" data-jc-config="required:true" class="m mt10">DB collection name</div>
	<div data-jc="dropdown" data-jc-path="method" data-jc-config="required:true;items: insert, update" class="m">@(Method)</div>
	<div data-jc="visible" data-jc-path="method" data-jc-config="if:value === 'insert'">
		<div data-jc="checkbox" data-jc-path="addid">Add unique ID to data before insert</div>
    </div>
</div>
<script>
	var mongoConfig = { connections: [] };
	ON('open.mongodbsave', function(component, options) {
		TRIGGER('mongo.connections', 'mongoConfig.connections');
	});
	ON('save.mongodbsave', function(component, options) {
	!component.name && (component.name =  options.collection + '/' + options.method);
	});
</script>`;

exports.install = function (instance) {

    instance.custom.reconfigure = function () {
        if (!instance.options.connector)
            return instance.status('No connector', 'red');

        if (instance.options.connector) {
            return instance.status('Configured', 'white');

        }

        instance.status('Not configured', 'red');
    };

    instance.on('options', instance.custom.reconfigure);

    instance.on('data', function (flowdata) {
        var options = instance.options;
        var MongoClient = MONGODB.connector(instance.options.connector);

        if (!MongoClient) {
            return instance.error(new Error('No connector'));
        }

        MongoClient = MongoClient.dbConnection;

        MongoClient.connect(function (err, database) {
            if (err) return instance.error(err);
            const base = database.db(database.s.options.dbName);

            if (options.method === 'insert') {
                base.collection(options.collection).insert(flowdata.data, function (err, result) {
                    if (err) return instance.error(err);
                    flowdata.data = { insetedIds: result.insertedIds };
                    instance.send2(0, flowdata);

                    database.close();
                })
            } else if (options.method === 'update') {
                base.collection(options.collection).update({ "_id": new Object_Id(flowdata.data.id) }, flowdata.data.data, function (err, result) {
                    if (err) return instance.error(err);

                    if (result.nModified == 1) {
                        flowdata.data = 'Update Success';
                        instance.send2(0, flowdata);
                    } else {
                        flowdata.data = result;
                        instance.send2(0, flowdata);
                    }

                    database.close();
                })
            }
        });
        instance.send2(1, flowdata);
    });


    instance.on('close', function () {
        // MONGODB.remove(instance.options.id);
        OFF('mongo.connections.status', connectorStatus);
    });

    ON('mongo.connections.status', connectorStatus);

    function connectorStatus(status, connectorId, msg) {
        if (connectorId !== instance.options.connector)
            return;

        switch (status) {
            case 'connecting':
                instance.status('Connecting', '#a6c3ff');
                break;
            case 'connected':
                instance.status('Connected', 'green');
                break;
            case 'disconnected':
                instance.status('Disconnected', 'red');
                break;
            case 'connectionfailed':
                instance.status('Connection failed', 'red');
                break;
            case 'new':
            case 'removed':
                instance.custom.reconfigure();
                break;
            case 'error':
                instance.status(msg, 'red');
                break;
            case 'reconfigured':
                instance.options.connector = msg;
                instance.reconfig();
                instance.custom.reconfigure();
                break;
        }
    }

    instance.custom.reconfigure();
}