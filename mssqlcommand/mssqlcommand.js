exports.id = 'mssqlcommand';
exports.title = 'MSSQL Command';
exports.version = '1.0.0';
exports.group = 'Databases';
exports.author = 'Leonardo Alves';
exports.color = '#D770AD';
exports.input = true;
exports.output = 1;
exports.options = {};
exports.readme = `# MSSQL Command

## Outputs


`;

exports.html = `
<div class="padding">
    <div data-jc="dropdown" data-jc-path="connector" data-jc-config="datasource:sqlConfig.connections;required:true" class="m">@(Connectors)</div>
</div>
<script>
	var sqlConfig = { connections: [] };
	ON('open.mssqlcommand', function(component, options) {
		TRIGGER('sql.connections', 'sqlConfig.connections');
	});
	ON('save.mssqlcommand', function(component, options) {
	});
</script>`;

exports.install = function (instance) {

    // instance.custom.reconfigure = function () {

    //     if (!instance.options.connector)
    //         return instance.status('No connector', 'red');

    //     if (instance.options.connector) {
    //         return instance.status('Configured', 'white');

    //     }

    //     instance.status('Not configured', 'red');
    // };

    instance.on('options', instance.custom.reconfigure);

    instance.on('data', function (flowdata) {
        var o = instance.options;

        var SqlClient = MSSQL.connector(instance.options.connector);
        var conn = SqlClient.dbConnection;
        conn.request().query(flowdata.data.query)
            .then(res => {
                flowdata.data = res;
                instance.send2(0, flowdata);
            })
            .catch(err => console.error(err));
    });

    // instance.on('close', function () {
    //     MSSQL.remove(instance.options.connector, instance.id);
    //     OFF('mongo.connections.status', connectorStatus);
    // });

    //ON('sql.connections.status', connectorStatus);

    function connectorStatus(status, connectorId, msg) {
        if (connectorId !== instance.options.connector)
            return;

        switch (status) {
            case 'connecting':
                instance.status('Connecting', '#a6c3ff');
                break;
            case 'connected':
                instance.status('Connected', 'green');
                break;
            case 'disconnected':
                instance.status('Disconnected', 'red');
                break;
            case 'connectionfailed':
                instance.status('Connection failed', 'red');
                break;
            case 'new':
            case 'removed':
                instance.custom.reconfigure();
                break;
            case 'error':
                instance.status(msg, 'red');
                break;
            case 'reconfigured':
                instance.options.connector = msg;
                instance.reconfig();
                instance.custom.reconfigure();
                break;
        }
    }

    //instance.custom.reconfigure();
}