exports.id = 'mssqlconnector';
exports.title = 'MS SQL Connector';
exports.version = '1.0.0';
exports.group = 'Databases';
exports.author = 'Leonardo Alves';
exports.color = '#D770AD';
exports.input = false;
exports.output = 0;
exports.options = { host: '127.0.0.1', port: '1433' };
exports.readme = `# MS SQL Connector`;
exports.npm = ['mssql']

exports.html = `
<div class="padding">
    <div data-jc="textbox" data-jc-path="host" data-jc-config="required:true" class="m mt10">Host name</div>
    <div data-jc="textbox" data-jc-path="port" data-jc-config="required:true" class="m mt10">Port</div>
    <div data-jc="textbox" data-jc-path="database" data-jc-config="required:true" class="m mt10">DB name</div>
    <br />
	<section>
		<label><i class="fa fa-lock"></i>@(Server authentication)</label>
		<div class="padding npb">
			<div class="row">
				<div class="col-md-6 m">
					<div data-jc="textbox" data-jc-path="username">@(User)</div>
				</div>
				<div class="col-md-6 m">
					<div data-jc="textbox" data-jc-path="userpassword" data-jc-config="type:password">@(Password)</div>
				</div>
			</div>
		</div>
	</section>
	<br />
</div>`;


var SqlClients = [];

global.MSSQL = {};


exports.install = function (instance) {

    var dbConnection;

    var SQLClient = require('mssql');

    instance.custom.reconfigure = function (o, old_options) {

        // if (old_options)
        //     MongoClients = MongoClients.remove(function (cn) {
        //         return cn.id === old_options.id;
        //     });

        var options = instance.options;

        if (!options.host || !options.port) {
            instance.status('Not configured', 'red');
            return;
        }

        options.id = `${(options.username || '')}@${options.host}:${options.port}/${options.database}`;

        if (dbConnection) {
            JSON.stringify(options) !== JSON.stringify(old_options) && dbConnection.close();
            EMIT('sql.connections.status', 'reconfigured', old_options.id, options.id);
        }

        instance.custom.createConnection();
    };
    
    instance.custom.createConnection = function () {

        ON('sql.connections.status', connectionStatus);

        var o = instance.options;
        var opts = { id: o.id };

        const config = {
            user: o.username,
            password: o.userpassword,
            server: o.host, // You can use 'localhost\\instance' to connect to named instance
            port: o.port,
            database: o.database,
         
            options: {
                encrypt: false // Use this if you're on Windows Azure
            }
        };
        SQLClient.connect(config)
            .then(conn => {
                console.log("conectou!");
                opts.dbConnection = conn
            })
            .catch(err => console.log("erro! " + err));
        SqlClients.push(opts);

        instance.status('Ready', 'white');
    };


    instance.close = function (done) {
        SqlClients = SqlClients.remove('id', instance.options.id);
        EMIT('sql.connections.status', 'removed', instance.options.id);
        OFF('sql.connections.status', connectionStatus);
        done();
    };

    function connectionStatus(status, connectionId, err) {
        if (connectionId !== instance.options.id)
            return;

        switch (status) {
            case 'connecting':
                instance.status('Connecting', '#a6c3ff');
                break;
            case 'connected':
                instance.status('Connected', 'green');
                break;
            case 'disconnected':
                instance.status('Disconnected', 'red');
                break;
            case 'connectionfailed':
                instance.status('Connection failed', 'red');
                break;
            case 'error':
                instance.error('Mongo Error, ID: ' + instance.id + '\n  ' + err);
                break;
        }
    }

    instance.on('options', instance.custom.reconfigure);
    instance.custom.reconfigure();
}

//console.log(FLOW);
FLOW.trigger('sql.connections', function (next) {
    var connections = [''];
    SqlClients.forEach(n => connections.push(n.id));
    next(connections);
});

MSSQL.connector = function (connectionId) {
    return SqlClients.findItem('id', connectionId);
};

MSSQL.connect = function (connectionId) {
    var connection = MSSQL.connector(connectionId);
    if (connection) {
        return connection.dbConnection.connect();
    } else {
        EMIT('sql.connections.status', 'error', connectionId, 'No such connector');
    }
};
