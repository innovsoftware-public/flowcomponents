exports.id = 'rabbitmqconnector';
exports.title = 'RabbitMQ Connector';
exports.version = '1.0.1';
exports.group = 'RabbitMQ';
exports.author = 'Leonardo Alves';
exports.color = '#888600';
exports.icon = 'clock-o';
exports.input = false;
exports.output = 0;
exports.options = { host: '127.0.0.1', port: '5672' };
exports.readme = `# RabbitMQ Connector`

exports.html = `<div class="padding">
	<section>
		<label><i class="fa fa-exchange"></i>@(Broker)</label>
		<div class="padding npb">
			<div class="row">
				<div class="col-md-6">
					<div data-jc="textbox" data-jc-path="host" data-jc-config="placeholder:127.0.0.1;required:true" class="m">Hostname or IP address</div>
				</div>
				<div class="col-md-6">
					<div data-jc="textbox" data-jc-path="port" data-jc-config="placeholder:5672;required:true" class="m">Port</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div data-jc="textbox" data-jc-path="username" class="m">Username</div>
				</div>
				<div class="col-md-6">
					<div data-jc="textbox" data-jc-path="password" data-jc-config="type:password" class="m">Password</div>
				</div>
			</div>
		</div>
	</section>
</div>`;

var MQTT_BROKERS = [];
var MQTT_CHANNELS = [];
global.RABBITMQ = {};

exports.install = function (instance) {

    var broker;
    AMQP = require('amqplib');

    instance.custom.reconfigure = function (o, old_options) {
        var options = instance.options;

        if (!options.host || !options.port) {
            instance.status('Not configured', 'red');
            return;
        }

        options.id = `${(options.username || '')}@${options.host}:${options.port}`;
        instance.custom.createConnection();
    };

    instance.custom.createConnection = function () { //instance.custom.createBroker
        ON('rabbitmq.connections.status', connectionStatus);

        var o = instance.options;
        var opts = { id: o.id };
        if (o.username) {
            var url = `amqp://${o.username}:${o.password}@${o.host}:${o.port}`;
        } else {
            var url = `amqp://${o.host}:${o.port}`;
        }

        opts.mqConnection = url;
        broker = new Broker(opts);
        MQTT_BROKERS.push(broker);

        //Criar um canal
        // var canal;
        // AMQP.connect(broker.mqConnection)
        //     .then(function(conn) {
        //         return conn.createChannel();
        //     }).then(function(ch){
        //         canal = ch;
        //         canal['id'] = opts.id;
        //         MQTT_CHANNELS.push(ch);
                
        //     }).catch(function(err){
        //         console.log(err);
        //         instance.error(err);
        //     });

        instance.status('Ready', 'white');
    };

    function connectionStatus(status, connectionId, err) {
        if (connectionId !== instance.options.id)
            return;

        switch (status) {
            case 'connecting':
                instance.status('Connecting', '#a6c3ff');
                break;
            case 'connected':
                instance.status('Connected', 'green');
                break;
            case 'disconnected':
                instance.status('Disconnected', 'red');
                break;
            case 'connectionfailed':
                instance.status('Connection failed', 'red');
                break;
            case 'error':
                instance.error('RabbitMQ Error, ID: ' + instance.id + '\n  ' + err);
                break;
        }
    }

    instance.on('options', instance.custom.reconfigure);
    instance.custom.reconfigure();
}

FLOW.trigger('rabbitmq.connections', function (next) {
    var connections = [''];
    MQTT_BROKERS.forEach(n => connections.push(n.id));
    next(connections);
});
FLOW.trigger('rabbitmq.channels', function (next) {
    var channels = [''];
    MQTT_BROKERS.forEach(n => channels.push(n));
    next(channels);
});

RABBITMQ.connector = function (connectionId) {
    return MQTT_BROKERS.findItem('id', connectionId);
};
RABBITMQ.channel = function(connectionId){
    return MQTT_CHANNELS.findItem('id', connectionId);
}

RABBITMQ.connect = function (connectionId) {
    var connection = RABBITMQ.connector(connectionId);
    if (connection) {
        return connection.mqConnection;
    } else {
        EMIT('rabbitmq.connections.status', 'error', connectionId, 'No such connector');
    }
};

function Broker(options) {
	var self = this;

	if (!options.mqConnection)
		return false;

	self.connecting = false;
	self.connected = false;
	self.closing = false;
	self.components = [];
	self.subscribtions = {};
    self.id = options.id;
    self.mqConnection = options.mqConnection;
	self.options = options;
	setTimeout(function() {
		EMIT('rabbitmq.connections.status', 'new', self.id);
	}, 500);
	return self;
};