exports.id = 'rabbitmqpublish';
exports.title = 'RabbitMQ Publish';
exports.version = '1.0.1';
exports.group = 'RabbitMQ';
exports.author = 'Leonardo Alves';
exports.color = '#888600';
exports.icon = 'clock-o';
exports.input = true;
exports.output = ['#702E2E', '#304782'];
exports.readme = `# RabbitMQ Publish`;
exports.npm = ['amqplib']

exports.html = `<div class="padding">
	<div data-jc="dropdown" data-jc-path="broker" data-jc-config="datasource:rabbitmqoConfig.connections;required:true" class="m">@(Brokers)</div>
	<div data-jc="textbox" data-jc-path="topic" data-jc-config="placeholder:hello/world";required:true" class="m">Topic</div>
</div>
<script>
	var rabbitmqoConfig = { connections: [] };
	ON('open.rabbitmqpublish', function(component, options) {
		TRIGGER('rabbitmq.connections', 'rabbitmqoConfig.connections');
    });
    ON('save.rabbitmqpublish', function(component, options) {
		!component.name && (component.name = 'amqp://' + options.broker);
	});
</script>`;

exports.install = function (instance) {

    AMQP = require('amqplib');

    instance.custom.reconfigure = function () {

        if (!instance.options.broker)
            return instance.status('No connector', 'red');

        if (instance.options.broker) {
            return instance.status('Configured', 'white');

        }

        instance.status('Not configured', 'red');
    };

    instance.on('options', instance.custom.reconfigure);

    instance.on('data', function (flowdata) {

        try {
            var options = instance.options;
            
            if (!instance.options.broker) {
                return instance.error(new Error('No connector'));
            } else {
                instance.status('Configured', 'white');
            }

            var server = RABBITMQ.connector(instance.options.broker);
            var url = server.mqConnection;
            var open = AMQP.connect(url);
    
            var q = instance.options.topic;
            open.then(function(conn) {
                return conn.createConfirmChannel();
              // return conn.createChannel();
            }).then(function(ch) {

              return ch.assertQueue(q).then(function(ok) {
                var retorno = false;
                retorno = ch.sendToQueue(q, new Buffer( JSON.stringify(flowdata.data)));
                //ch.sendToQueue(q, new Buffer( JSON.stringify( {nome: 'Diego', id:  148932748979 })));

                instance.send2(0, retorno);
                ch.close();
                return retorno;
              });
              
            }).catch(function(error){
                console.log(error);
                instance.error(error);
            });
        } catch (error) {
            console.log(error);
            instance.error(error);
        }
        //instance.send2(0, flowdata.data);
    });

    ON('rabbitmq.connections.status', brokerstatus);

	function brokerstatus(status, brokerid, msg) {
		if (brokerid !== instance.options.broker)
			return;

		switch (status) {
			case 'connecting':
				instance.status('Connecting', '#a6c3ff');
				break;
			case 'connected':
				// re-subscibe on reconnect
				//RABBITMQ.subscribe(instance.options.broker, instance.id, instance.options.topic);
				instance.status('Connected', 'green');
				break;
			case 'disconnected':
				instance.status('Disconnected', 'red');
				break;
			case 'connectionfailed':
				instance.status('Connection failed', 'red');
				break;
			case 'new':
			case 'removed':
				instance.custom.reconfigure();
				break;
			case 'error':
				instance.status(msg, 'red');
				break;
			case 'reconfigured':
                instance.options.broker = msg;
				instance.custom.reconfigure();
				break;
		}
    }

}