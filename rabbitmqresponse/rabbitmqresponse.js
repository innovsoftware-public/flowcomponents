exports.id = 'rabbitmaresponse';
exports.title = 'RabbitMQ Response';
exports.version = '1.0.1';
exports.group = 'RabbitMQ';
exports.author = 'Leonardo Alves';
exports.color = '#888600';
exports.icon = 'clock-o';
exports.input = true;
exports.output = ['#666D76'];
exports.readme = `# RabbitMQ Response
## Input:
- Expects data to be an Object with \`response mode\`:
\`\`\`javascript
{
    responseMode: 'ack',  // response mode expects one of the following values: ack, nack or reject.
}
\`\`\`

## Output:
RabbitMQ Response will respond with a message that will contain the \`response mode\` recieved from the previous step.
`;

exports.html;

exports.install = function (instance) {

    instance.on('data', function(flowdata){
        try {
            var msg = flowdata.repository.msg;
            var responseMode = flowdata.data.responseMode;
            var ch = flowdata.repository.channel;
            var outputMessage = 'No message';
            if (msg && msg !== false && ch) {
                switch(responseMode) {
                    case 'nack':
                        ch.nack(msg);
                        outputMessage = 'Nack message';
                        break;
                    case 'reject':
                        ch.reject(msg);
                        outputMessage = 'Reject message';
                        break
                    default:
                        ch.ack(msg);
                        outputMessage = 'Ack message';
                        break;
                }
            }
            flowdata.data = outputMessage
            instance.send2(flowdata);
        } catch (error) {
            console.log(error);
        }
    });
    instance.custom.reconfigure = function (o, old_options) {

    };

    instance.on('options', instance.custom.reconfigure);
    instance.on('close', function() {

    });

    instance.custom.reconfigure();

}