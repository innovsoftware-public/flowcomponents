exports.id = 'rabbitmqsubscribe';
exports.title = 'RabbitMQ Subscribe';
exports.version = '1.0.1';
exports.group = 'RabbitMQ';
exports.author = 'Leonardo Alves';
exports.color = '#888600';
exports.icon = 'clock-o';
exports.input = false;
exports.output = 1;
exports.readme = `# RabbitMQ Subscribe`;
exports.npm = ['amqplib']

exports.html = `<div class="padding">
    <div data-jc="dropdown" data-jc-path="broker" data-jc-config="datasource:rabbitmqoConfig.connections;required:true" class="m">@(Brokers)</div>
    <div data-jc="textbox" data-jc-path="topic" data-jc-config="placeholder:hello/world";required:true" class="m">Topic</div>
    <div data-jc="textbox" data-jc-path="prefetch" data-jc-config="type:number;increment:true;align:left" class="m">@(Prefetch (defaults to 1))</div>
    <br />
    <div data-jc="checkbox" data-jc-path="trueresponse" class="b black">@(Automatically respond with 'Ack'?)</div>
    <div class="help m">@(If not checked you need to use RabbitMQ Response component to respond to the subscribe.)</div>
    <hr />
</div>
<script>
    var rabbitmqoConfig = { connections: [], channels: [] };
	ON('open.rabbitmqsubscribe', function(component, options) {
        TRIGGER('rabbitmq.connections', 'rabbitmqoConfig.connections');
    });
    ON('save.rabbitmqsubscribe', function(component, options) {
		!component.name && (component.name = 'amqp://' + options.broker);
	});
</script>`;

exports.install = function (instance) {

    AMQP = require('amqplib');

    instance.reconfigure = function (o, old_options) {
        
        if(instance.channel){
            instance.channel.close();
        }

        if (!instance.options.broker)
            return instance.status('No connector', 'red');

        if (instance.options.broker) {
            try {
                var options = instance.options;
                
                if (!instance.options.broker) {
                    return instance.error(new Error('No connector'));
                } else {
                    instance.status('Configured', 'white');
                }
                
                var self = this;
                
                var server = RABBITMQ.connector(instance.options.broker);
                var url = server.mqConnection;
                var open = AMQP.connect(url);        
                var q = instance.options.topic;

                open.then(function(conn) {
                    return conn.createChannel();
                  }).then(function(ch) {
                    instance.channel = ch;
                    return ch.assertQueue(q).then(function(ok) {
                        if(instance.options.prefetch > 0){
                            ch.prefetch(instance.options.prefetch);
                        }else {
                            ch.prefetch(1);
                        }

                      var consumidor = ch.consume(q, function(msg) {
                        if (msg !== null) {
                            var t = JSON.parse(msg.content);

                            var data = instance.make({
                                body: t,
                                session: self.session,
                                msg: msg
                            });                            
                            if (instance.options.trueresponse) {
                                ch.ack(msg);
                                instance.send2(0, t);
                                return;
                            }
                            data.repository.msg = msg;
                            data.repository.channel = instance.channel;                    
                            //ch.ack(msg); //retira da fila
                            //ch.close();
                            instance.send2(0, data);
                        }
                      }, { "consumerTag": "flow." + instance.id, noAck: false});
                      return consumidor;
                    });
                }).catch(function(error){
                    console.log(error);
                    instance.error(error);
                });
            } catch (error) {
                console.log(error);
                instance.error(error);
            }
        }
    };

    instance.on('options', instance.reconfigure);

    instance.on('close', function() {
        if(instance.channel){
            instance.channel.close();
        }
    });

    ON('rabbitmq.connections.status', brokerstatus);

	function brokerstatus(status, brokerid, msg) {
		if (brokerid !== instance.options.broker)
			return;

		switch (status) {
			case 'connecting':
				instance.status('Connecting', '#a6c3ff');
				break;
			case 'connected':
				instance.status('Connected', 'green');
				break;
			case 'disconnected':
				instance.status('Disconnected', 'red');
				break;
			case 'connectionfailed':
				instance.status('Connection failed', 'red');
				break;
			case 'new':
			case 'removed':
				instance.reconfigure();
				break;
			case 'error':
				instance.status(msg, 'red');
				break;
			case 'reconfigured':
                instance.options.broker = msg;
				instance.reconfigure();
				break;
		}
    }
    
    ON('rabbitmq.connections.message', message);

    function message(brokerid, connection) {
		if (brokerid !== instance.options.broker)
			return;

            var key;
            var self = this;
            
            var q = instance.options.topic;
            ch = connection;
            ch.assertQueue(q).then(function(ok) {
                ch.prefetch(instance.options.prefetch || 1);
                return ch.consume(q, function(msg) {

                    if (instance.options.trueresponse) {
                        ch.ack(msg);
                        return;
                    }
                    if (msg !== null) {
                        var t = JSON.parse(msg.content);

                        var data = instance.make({
                            body: t,
                            session: self.session,
                            msg: msg
                        });
                        //data.repository.controller = self;                        
                        instance.send2(0, data);
                        ch.close();
                    }
                  });
            });

            // var server = RABBITMQ.connector(instance.options.broker);
            // var url = server.mqConnection;
            // var open = AMQP.connect(url);
    
            
            // open.then(function(conn) {
            //     return conn.createChannel();
            //   }).then(function(ch) {
            //     return ch.assertQueue(q).then(function(ok) {
            //       ch.prefetch(instance.options.prefetch || 1);
            //       return ch.consume(q, function(msg) {

            //         if (instance.options.trueresponse) {
            //             ch.ack(msg);
            //             return;
            //         }
            //         if (msg !== null) {
            //             var t = JSON.parse(msg.content);

            //             var data = instance.make({
            //                 body: t,
            //                 session: self.session,
            //                 msg: msg
            //             });
            //             data.repository.controller = self;                        
            //             instance.send2(0, data);
            //             ch.close();
            //         }
            //       });
            //     });
            // }).catch(function(error){
            //     console.log(error);
            //     instance.error(error);
            // });
    }
    
    instance.reconfigure();

}