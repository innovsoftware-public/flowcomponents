exports.id = 'smsinnov';
exports.title = 'SMS Innov';
exports.group = 'Notifications';
exports.color = '#8CC152';
exports.input = true;
exports.author = 'Leonardo Alves';
exports.icon = 'commenting-o';

exports.html = `<div class="padding">
	<div class="row">
		<div class="col-md-6 m">
			<div data-jc="textbox" data-jc-path="key" class="m" data-jc-config="required:true;maxlength:30;type:password">@(NEXMO Key)</div>
		</div>
		<div class="col-md-6 m">
			<div data-jc="textbox" data-jc-path="secret" class="m" data-jc-config="required:true;maxlength:35;type:password">@(NEXMO Secret)</div>
		</div>
	</div>
</div>`;

exports.readme = `# SMS sender

The component has to be configured. Sender uses [NEXMO API provider](https://www.nexmo.com). 
This component expects data to be an Object with \`message\`, \`sender\` and \`target\`
\`\`\`javascript
{
    "message": "Hello, this is my message text",
    "sender": "sender_name",
    "targer": "phone_number"
}
\`\`\``;

exports.install = function(instance) {

	var can = false;

	instance.on('data', function(response) {
		can && instance.custom.send(response.data);
	});

	instance.custom.send = function(data) {
        sender = data.sender;
        target = data.target;
        message = data.message;
		RESTBuilder.make(function(builder) {
			builder.url('https://rest.nexmo.com/sms/json?api_key={0}&api_secret={1}&from={2}&to={3}&text={4}&type=unicode'.format(instance.options.key, instance.options.secret, encodeURIComponent(sender), target, encodeURIComponent(typeof(message) === 'object' ? JSON.stringify(message) : message.toString())));
			builder.exec(function(err, response) {
				LOGGER('sms', 'response:', JSON.stringify(response), 'error:', err);
			});
		});
	};

	instance.reconfigure = function() {
		can = instance.options.key && instance.options.secret ? true : false;
		instance.status(can ? '' : 'Not configured', can ? undefined : 'red');
	};

	instance.on('options', instance.reconfigure);
	instance.reconfigure();
};