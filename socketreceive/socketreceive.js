exports.id = 'socketreceive';
exports.title = 'SOCKET Receive';
exports.group = 'SOCKET';
exports.color = '#5D9CEC';
exports.icon = 'globe';
exports.input = false;
exports.output = ['#6CAC5A'];
exports.version = '1.0.0';
exports.author = 'Diego Amaral';
exports.cloning = false;
exports.options = { url: ''};
exports.readme = `# SOCKET Receive

__Output__:
-  Forwards object received in \`client.on('message',data)\`:

__SOCKET Receive__:
-  this componte connects to the socket informed and waits for messages:
`;

exports.html = `<div class="padding">
	<section>
		<label>@(Main settings)</label>
		<div class="padding npb">
			<div data-jc="textbox" data-jc-path="url" class="m" data-jc-config="required:true;maxlength:500;placeholder:ws://server/socket">@(URL address)</div>
			
		</div>
	</section>
	<br />
	<div data-jc="keyvalue" data-jc-path="headers" data-jc-config="placeholderkey:@(Header name);placeholdervalue:@(Header value and press enter)" class="m">@(Custom headers)</div>
	<div data-jc="keyvalue" data-jc-path="cookies" data-jc-config="placeholderkey:@(Cookie name);placeholdervalue:@(Cookie value and press enter)">@(Cookies)</div>
	</div>
	
<script>
	

	ON('save.socketreceive', function(component, options) {
		!component.name && (component.name = 'Receive: ' + options.url);
	});
</script>`; `<div class="padding">
	<section>
		<label>@(Main settings)</label>
		<div class="padding npb">
			<div data-jc="textbox" data-jc-path="url" class="m" data-jc-config="required:true;maxlength:500;placeholder:ws://server/socket">@(URL address)</div>
			
		</div>
	</section>
	<br />
	<div data-jc="keyvalue" data-jc-path="headers" data-jc-config="placeholderkey:@(Header name);placeholdervalue:@(Header value and press enter)" class="m">@(Custom headers)</div>
	<div data-jc="keyvalue" data-jc-path="cookies" data-jc-config="placeholderkey:@(Cookie name);placeholdervalue:@(Cookie value and press enter)">@(Cookies)</div>
	</div>
	
<script>
	

	ON('save.socketreceive', function(component, options) {
		!component.name && (component.name = 'Receive: ' + options.url);
	});
</script>`;

exports.install = function (instance) {

	var id;



	instance.reconfigure = function () {

		var options = instance.options;

		if (!options.url) {
			instance.status('Not configured', 'red');
			return;
		}

		id && UNINSTALL('socket', id);
		id = 'id:' + instance.id;

		var client = instance.custom.webSocket;
		var closeClient = function (client) {
			client.options.reconnect = 0;
			client.close();
		}

		client && closeClient(client);

		instance.custom.webSocket = WEBSOCKETCLIENT(function (client) {

			client.connect(options.url);

			client.on('open', function () {
				instance.status('Listening', 'green');
			});

			client.on('close', function () {
				//console.log('close');
				instance.status('closed', 'red');
			});

			client.on('message',function(data){
				instance.send2(data);
			})

			return client;
		});
		if(!instance.custom.webSocket.closed){
			instance.status('Listening', 'green');
		}
	};



	instance.reconfigure();
	instance.on('options', instance.reconfigure);

	instance.on('close', function () {
		id && UNINSTALL('socket', id);
	});

	instance.on('click', function () {

		instance.reconfigure();
		instance.save();
	});



};