exports.id = 'socketregister';
exports.title = 'SOCKET Register';
exports.group = 'SOCKET';
exports.color = '#5D9CEC';
exports.icon = 'globe';
exports.input = false;
exports.output = ['#6CAC5A', '#37BC9B'];
exports.version = '1.0.0';
exports.author = 'Diego Amaral';
exports.cloning = false;
exports.modelOrigem = 'httproute_V1.1.0';
exports.options = { method: 'GET', url: '', size: 5, cacheexpire: '5 minutes', cachepolicy: 0, timeout: 5 };
exports.readme = `# HTTP route

__Outputs__:
- first output: raw data (cache is empty or is disabled)
- second output: cached data

When a request comes in bellow object is available at \`flowdata.data\`:

\`\`\`javascript
{
	params: { id: '1' },     // params for dynamic routes, e.g. /test/{id}
	query: { msg: 'Hello' }, // parsed query string, e.g. /test/1?msg=Hello
	body: { test: 'OK' },    // object if json requests otherwise string
	headers: {},             // headers data
	session: {},             // session data
	user: {},                // user data
	files: [],               // uploaded files
	url: '/users/',          // a relative URL address
	referrer: '/',           // referrer
	mobile: false,           // determines mobile device
	robot: false,            // determines search robots/crawlsers
	language: 'en'           // determines language
}
\`\`\`

See [documentation for flags](https://docs.totaljs.com/latest/en.html#api~HttpRouteOptionsFlags~unauthorize). These method flags are set automatically e.g. \`get, post, put or delete\`

---

\`id:ROUTE_ID\` flag cannot be used since it's already used by this component internally`;

exports.html = `<div class="padding">
	<section>
		<label>@(Main settings)</label>
		<div class="padding npb">
			<div data-jc="textbox" data-jc-path="url" class="m" data-jc-config="required:true;maxlength:500;placeholder:/api/test">@(URL address)</div>
			<div data-jc="dropdown" data-jc-path="method" data-jc-config="required:true;items:,GET,POST,PUT,DELETE,OPTIONS" class="m">@(HTTP method)</div>

			<div class="row">
				<div class="col-md-6 m">
					<div data-jc="textbox" data-jc-path="flags" data-jc-config="placeholder:json">@(Additional flags)</div>
					<div class="help m">@(Separate flags by comma e.g. <code>json, authorize</code>)</div>
				</div>
				<div class="col-md-3 m">
					<div data-jc="textbox" data-jc-path="size" data-jc-config="placeholder:@(in kB);increment:true;type:number;maxlength:10;align:center">@(Max. request size)</div>
					<div class="help m">@(In <code>kB</code> kilobytes)</div>
				</div>
				<div class="col-md-3 m">
					<div data-jc="textbox" data-jc-path="timeout" data-jc-config="placeholder:@(in seconds);increment:true;type:number;maxlength:5;align:center">@(Timeout)</div>
					<div class="help m">@(In seconds.)</div>
				</div>
			</div>
		</div>
	</section>
	<br />
	<div data-jc="checkbox" data-jc-path="emptyresponse" class="b black">@(Automatically respond with 200 OK?)</div>
	<div class="help m">@(If not checked you need to use HTTP response component to respond to the request.)</div>
	<hr />
	<div data-jc="keyvalue" data-jc-path="headers" data-jc-config="placeholderkey:@(Header name);placeholdervalue:@(Header value and press enter)" class="m">@(Custom headers)</div>
	<div data-jc="keyvalue" data-jc-path="cookies" data-jc-config="placeholderkey:@(Cookie name);placeholdervalue:@(Cookie value and press enter)">@(Cookies)</div>
	</div>
	<hr class="nmt" />
	<div class="padding npt">
		<div class="row">
			<div class="col-md-9 m">
				<div data-jc="dropdown" data-jc-path="cachepolicy" data-jc-config="type:number;items:@(no cache)|0,@(URL)|1,@(URL + query string)|2,@(URL + query string + user instance)|3">@(Cache policy)</div>
				<div class="help">@(User instance must contain <code>id</code> property.)</div>
			</div>
			<div class="col-md-3 m">
				<div data-jc="textbox" data-jc-path="cacheexpire" data-jc-config="maxlength:20;align:center">@(Expiration)</div>
				<div class="help">@(E.g. <code>5 minutes</code>)</div>
			</div>
		</div>
	</div>
<script>
	ON('open.socketregister', function(component, options) {
		if (options.flags instanceof Array) {
			var method = options.method.toLowerCase();
			options.flags = options.flags.remove(function(item) {
				switch (typeof(item)) {
					case 'string':
						return item.substring(0, 3) === 'id:' || item === method;
					case 'number': // timeout
						return true;
				}
			}).join(', ');
		}
	});

	ON('save.socketregister', function(component, options) {
		!component.name && (component.name = options.method + ' ' + options.url);
	});
</script>`;

exports.install = function (instance) {

	var id;

	instance.custom.emptyresponse = function (self) {
		self.plain();
	};

	instance.reconfigure = function () {

		var options = instance.options;

		if (!options.url) {
			instance.status('Not configured', 'red');
			return;
		}

		if (typeof (options.flags) === 'string')
			options.flags = options.flags.split(',').trim();

		id && UNINSTALL('route', id);
		id = 'id:' + instance.id;

		var flags = options.flags || [];
		flags.push(id);
		flags.push(options.method.toLowerCase());
		options.timeout && flags.push(options.timeout * 1000);

		/*
		F.route(options.url, function () {

			var key;
			var self = this;

			if (instance.options.emptyresponse) {
				instance.status('200 OK');
				setTimeout(instance.custom.emptyresponse, 100, self);

				if (instance.hasConnection(0)) {
					var data = instance.make({
						query: self.query,
						body: self.body,
						session: self.session,
						user: self.user,
						files: self.files,
						headers: self.req.headers,
						url: self.url,
						params: self.params
					});
					instance.send2(0, data);
				}

				return;
			}

			switch (instance.options.cachepolicy) {
				case 1: // URL
					key = 'rro' + instance.id + self.url.hash();
					break;
				case 2: // URL + query
				case 3: // URL + query + user
					key = self.url;
					var keys = Object.keys(self.query);
					keys.sort();
					for (var i = 0, length = keys.length; i < length; i++)
						key += keys[i] + self.query[keys[i]] + '&';
					if (instance.options.cachepolicy === 3 && self.user)
						key += 'iduser' + self.user.id;
					key = 'rro' + instance.id + key.hash();
					break;
			}

			if (key && F.cache.get2(key)) {
				var data = instance.make(F.cache.get2(key));
				data.repository.controller = self;
				instance.send2(1, data);
			} else {

				var data = instance.make({
					query: self.query,
					body: self.body,
					session: self.session,
					user: self.user,
					files: self.files,
					headers: self.req.headers,
					url: self.url,
					params: self.params,
					mobile: self.mobile,
					robot: self.robot,
					referrer: self.referrer,
					language: self.language
				});

				data.repository.controller = self;
				instance.send2(0, data);
				key && FINISHED(self.res, () => F.cache.set(key, self.$flowdata.data, instance.options.cacheexpire));
			}

		}, flags, options.size || 5);

*/
		F.websocket(options.url, socket_homepage, ['json']);
		instance.status('Listening', 'green');
	};

	instance.reconfigure();
	instance.on('options', instance.reconfigure);

	instance.on('close', function () {
		id && UNINSTALL('route', id);
	});

	function socket_homepage() {

		var controller = this;
		controller.on('open', function (client) {
			instance.status(`Listening / Online: ${controller.online}`,'green' );
			//client.send({ message: 'Hello {0}'.format(client.id) });
			console.log(`New connection ${client.id} / Online: ${controller.online}`);
			instance.send2(0, { "cliente": client.id, "Status": "conectado" });
			//controller.send({ message: 'Connect new user: {0}\nOnline: {1}'.format(client.id, controller.online) }, null, [client.id]);
		});

		controller.on('close', function (client) {
			console.log('Disconnect / Online:', controller.online);
			instance.status(`Listening / Online: ${controller.online}`,'green' );
			//controller.send({ message: 'Disconnect user: {0}\nOnline: {1}'.format(client.id, controller.online) });
			instance.send2(0, { "cliente": client.id, "Status": "desconectado" });
		});
		controller.on('message', function (client, message) {
			console.log(message);
			console.log('mensages');
			controller.send(message);
		});
	}

};

