exports.id = 'twittersearch';
exports.title = 'Twitter Search';
exports.group = 'Twitter';
exports.color = '#1DA1F2';
exports.input = true;
exports.output = 1;
exports.author = 'Leonardo Alves';
exports.icon = 'random';
exports.npm = ['oauth', 'crypto'];

exports.html = `<div class="padding">
    <div data-jc="dropdown" data-jc-path="language" class="m" data-jc-config="items:,@(Chinese)|zh,@(English)|en,@(French)|json,@(German)|de,@(Japanese)|ja,@(Portuguese)|pt,@(Spanish)|es;required:true">@(Language)</div>
    <div class="padding bg-smoke">
        <section>
            <label><i class="fa fa-lock"></i>@(Authentication)</label>
            <div class="padding npb">
                <div class="row">
                    <div class="col-md-6 m">
                        <div data-jc="textbox" data-jc-path="consumer_key" data-jc-config="required:true">@(Consumer Key)</div>
                    </div>
                    <div class="col-md-6 m">
                        <div data-jc="textbox" data-jc-path="consumer_secret" data-jc-config="required:true">@(Consumer Secret)</div>
                    </div>
                    <div class="col-md-6 m">
                        <div data-jc="textbox" data-jc-path="access_token" data-jc-config="required:true">@(Access Token)</div>
                    </div>
                    <div class="col-md-6 m">
                        <div data-jc="textbox" data-jc-path="token_secret" data-jc-config="required:true">@(Access Token Secret)</div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>`;

exports.readme = `# Twitter Search

`;

exports.install = function(instance) {
    instance.custom.reconfigure = function () {
        if (instance.options.hash) {
            return instance.status('Configured', 'white');
        }

        instance.status('Not configured', 'red');
    };
	instance.on('data', function(flowdata) {
        var OAuth = require('OAuth');
        var crypto = require("crypto");

        var oauth = new OAuth.OAuth(
            'https://api.twitter.com/oauth/request_token',
            'https://api.twitter.com/oauth/access_token',
            instance.options.consumer_key,
            instance.options.consumer_secret,
            '1.0A',
            null,
            'HMAC-SHA1'
            );
    
        var encText = crypto.createHash(instance.options.hash).update(JSON.stringify(flowdata.data)).digest("hex");
        oauth.get(
            'https://api.twitter.com/1.1/search/tweets.json?q='+ encodeURI(flowdata.data.keyword) +'&lang='+ instance.options.language +'&result_type=recent&count=100',
            instance.options.access_token,
            instance.options.token_secret,
            function (e, data, res){
                if (e) console.error(e);        
                
                flowdata.data = {
                    queryId: encText,
                    data: data
                };
                instance.send2(flowdata);
            }
        );
	});

	instance.reconfigure = function() {

	};

	instance.on('options', instance.custom.reconfigure);
    instance.reconfigure();
    instance.custom.reconfigure();
};